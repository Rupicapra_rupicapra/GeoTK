from computations import *
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk as g

class MainWindow(g.Window):
    def __init__(self):
        g.Window.__init__(self, title='Rock Mass Rating')
        grid = g.Grid(row_spacing=4, column_spacing=8)
        self.add(grid)
        
        # RQD computation
        rqdAdjust = g.Adjustment(0, 0, 10000, 1, 10, 0)
        rqdAdjust2 = g.Adjustment(0, 0, 10000, 1, 10, 0)
        self.totalElts10 = g.SpinButton()
        self.totalElts10.set_adjustment(rqdAdjust)
        self.totalElts10.set_numeric(True)
        self.totalLength = g.SpinButton()
        self.totalLength.set_adjustment(rqdAdjust2)
        self.totalLength.set_numeric(True)
        self.rqd = g.Entry()
        self.rqd.set_editable(False)
        self.rqdRating = g.Entry()
        self.rqdRating.set_editable(False)
        #
        grid.attach(g.Label('Rock Quality Drill'), 0, 0, 4, 1)
        grid.attach(g.Label('Total length of elts > 10cm'), 0, 1, 1, 1)
        grid.attach(self.totalElts10, 1, 1, 1, 1)
        grid.attach(g.Label('Total length of drilling'), 2, 1, 1, 1)
        grid.attach(self.totalLength, 3, 1, 1, 1)
        grid.attach(g.Label('RQD value'), 0, 2, 1, 1)
        grid.attach(self.rqd, 1, 2, 1, 1)
        grid.attach(g.Label('RQD rating'), 2, 2, 1, 1)
        grid.attach(self.rqdRating, 3, 2, 1, 1)

        hid = self.totalElts10.connect('value-changed', self.updateRQD)
        hid2  = self.totalLength.connect('value-changed', self.updateRQD)

        ###########################
        # Discontinuities spacing #
        ###########################

        #sdAdjust = g.Adjustment(float(0.0), float(0.0), float(100.0), float(0.01), float(1), 1)
        sdAdjust = g.Adjustment(0.0, 0.0, 600.0, 0.01, 1.0, 1.0)
        self.sdisc = g.SpinButton()
        self.sdisc.set_adjustment(sdAdjust)
        self.sdisc.set_numeric(True)
        self.sdisc.set_digits(2)
        self.sdRating = g.Entry(editable=False)

        grid.attach(g.HSeparator(), 0, 3, 4, 1)
        grid.attach(g.Label('Discontinuities'), 0, 4, 4, 1)
        grid.attach(g.Label('Discontinuities spacing (m)'), 0, 5, 1, 1)
        grid.attach(self.sdisc, 1, 5, 1, 1)
        grid.attach(g.Label('Spacing rating'), 2, 5, 1, 1)
        grid.attach(self.sdRating, 3, 5, 1, 1)

        self.sdisc.connect('value-changed', self.updateSD)

        ##########################
        # Discontinuities length #
        ##########################
        dlAdjust = g.Adjustment(0, 0, 100, 0.1, 1, 0)
        self.dlen = g.SpinButton(numeric=True, adjustment=dlAdjust, digits=1)
        self.dlRating = g.Entry(editable=False)

        grid.attach(g.HSeparator(), 0, 6, 4, 1)
        grid.attach(g.Label('Discontinuities length (m)'), 0, 7, 1, 1)
        grid.attach(self.dlen, 1, 7, 1, 1)
        grid.attach(g.Label('Length rating'), 2, 7, 1, 1)
        grid.attach(self.dlRating, 3, 7, 1, 1)

        self.dlen.connect('value-changed', self.updateDL)

        #######################
        # Separation aperture #
        #######################
        saAdjust = g.Adjustment(0, 0, 100, 0.01, 1, 0)
        self.sAperture = g.SpinButton(numeric=True, adjustment=saAdjust, digits=2)
        self.saRating = g.Entry(editable=False, text='6')
        
        grid.attach(g.HSeparator(), 0, 8, 4, 1)
        grid.attach(g.Label('Separation aperture (cm)'), 0, 9, 1, 1)
        grid.attach(self.sAperture, 1, 9, 1, 1)
        grid.attach(g.Label('Aperture rating'), 2, 9, 1, 1)
        grid.attach(self.saRating, 3, 9, 1, 1)

        self.sAperture.connect('value-changed', self.updateSA)


    def updateRQD(self, widget):
        if self.totalLength.get_value() != 0:
            rqd = calcRQD(self.totalElts10.get_value(), self.totalLength.get_value())
            self.rqd.set_text(str(rqd))

            rqdRating = ratingRQD(rqd)
            self.rqdRating.set_text(str(rqdRating))

    def updateSD(self, widget):
        sd = spacingDisc(self.sdisc.get_value())
        self.sdRating.set_text(str(sd))

    def updateDL(self, widget):
        dl = discLen(self.dlen.get_value())
        self.dlRating.set_text(str(dl))

    def updateSA(self, widget):
        sa = separation(self.sAperture.get_value())
        self.saRating.set_text(str(sa))

if __name__ == '__main__':
    win = MainWindow()
    win.connect('delete-event', g.main_quit)
    win.show_all()
    g.main()
